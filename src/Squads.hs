-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.

module Squads
    ( -- * Squad
      Squad
    , squad
    , submit
    , submitAll
    , cease

      -- * Dispatcher
    , Dispatcher
    , mkDispatcher
    , dispatch
    , dispatchAll
    , shutdown
    ) where

import Control.Applicative
import Control.Concurrent (myThreadId)
import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Exception (SomeException, catch)
import Control.Monad
import Data.Foldable (toList)
import Data.Hashable
import Data.Sequence (Seq, (|>), (><))
import Data.Vector (Vector, (!))

import qualified Data.Sequence as Seq
import qualified Data.Vector   as Vec

-----------------------------------------------------------------------------
-- Dispatcher

data Dispatcher a = Dispatcher
    { squads :: Vector (Squad a)
    , size   :: Int
    , final  :: Squad a
    }

mkDispatcher :: Int
             -- ^ number of squads
             -> Int
             -- ^ total number of runners
             -> Int
             -- ^ maximum number of items per runner per cycle
             -> (SomeException -> IO ())
             -- ^ exception handler
             -> ([a] -> IO ())
             -- ^ runner action
             -> IO (Dispatcher a)
mkDispatcher nm nr ni onE f = do
    fin <- squad nr ni onE f
    sqs <- Vec.fromList <$> replicateM nm (squad 1 (2*ni) onE (submitAll fin))
    return $ Dispatcher sqs (Vec.length sqs) fin

dispatch :: Dispatcher a -> a -> IO ()
dispatch d a = do
    i <- myThreadId
    submit (squads d ! (hash i `mod` size d)) a

dispatchAll :: Dispatcher a -> [a] -> IO ()
dispatchAll d xs = do
    i <- myThreadId
    submitAll (squads d ! (hash i `mod` size d)) xs

shutdown :: Dispatcher a -> IO ()
shutdown d = do
    Vec.mapM_ cease (squads d)
    cease (final d)

-----------------------------------------------------------------------------
-- Squad

data Squad a = Squad
    { items   :: TVar (Seq a)
    , stop    :: TVar Bool
    , runners :: [Async ()]
    }

squad :: Int
      -- ^ number of runner threads
      -> Int
      -- ^ maximum number of items per runner per cycle
      -> (SomeException -> IO ())
      -- ^ exception handler
      -> ([a] -> IO ())
      -- ^ action to feed
      -> IO (Squad a)
squad nr ni onE f = do
    ch <- newTVarIO False
    it <- newTVarIO Seq.empty
    ws <- replicateM nr (async $ runner ch it)
    return $ Squad it ch ws
  where
    runner st it = do
        xs <- atomically $ do
            s <- readTVar st
            if s then
                return Seq.empty
            else do
                i <- readTVar it
                let (a, b) = Seq.splitAt ni i
                when (Seq.null a) $
                    retry
                writeTVar it b
                return a
        unless (Seq.null xs) $ do
            f (toList xs) `catch` onE
            runner st it

cease :: Squad a -> IO ()
cease p = do
    atomically $ writeTVar (stop p) True
    mapM_ wait (runners p)

submit :: Squad a -> a -> IO ()
submit t x = atomically $ modifyTVar (items t) (|> x)

submitAll :: Squad a -> [a] -> IO ()
submitAll t xs = atomically $ modifyTVar (items t) (>< (Seq.fromList xs))
