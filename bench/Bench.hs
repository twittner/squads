-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.

module Main (main) where

import Control.Concurrent
import Criterion
import Criterion.Main
import Squads

main :: IO ()
main = do
    s <- squad 8 16 print (const (return ()) :: [Int] -> IO ())
    d <- mkDispatcher 4 8 16 print (const (return ()) :: [Int] -> IO ())
    defaultMain
        [ bgroup "bench"
            [ bench "s 1  100"   $ nfIO (fs s 1   100)
            , bench "d 1  100"   $ nfIO (fd d 1   100)
            , bench "s 10 100"   $ nfIO (fs s 10  100)
            , bench "d 10 100"   $ nfIO (fd d 10  100)
            , bench "s 100 100"  $ nfIO (fs s 100 100)
            , bench "d 100 100"  $ nfIO (fd d 100 100)
            ]
        ]

fs :: Squad Int -> Int -> Int -> IO ()
fs s c n = mapM_ forkIO (replicate c (submit s n))

fd :: Dispatcher Int -> Int -> Int -> IO ()
fd d c n = mapM_ forkIO (replicate c (dispatch d n))
